package net.phyloviz.category;

public interface CategoryChangeListener {

	public void categoryChange(CategoryProvider cp);
}
